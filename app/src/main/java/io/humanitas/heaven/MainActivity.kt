package io.humanitas.heaven

import android.Manifest.permission.WRITE_EXTERNAL_STORAGE
import android.annotation.SuppressLint
import android.app.Activity
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Intent
import android.content.pm.PackageManager.PERMISSION_DENIED
import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.google.android.material.floatingactionbutton.FloatingActionButton
import io.humanitas.heaven.client.Client
import io.humanitas.heaven.client.HeavenCommands
import io.humanitas.heaven.client.HeavenReplies
import io.humanitas.heaven.python.PythonService
import java.io.File
import java.util.*
import kotlin.collections.ArrayList

class MainActivity : AppCompatActivity() {

    private var sv: ScrollView? = null
    private var lv: LinearLayout? = null
    private var fb: FloatingActionButton? = null
    private var pythonCommandsMulti: MultiAutoCompleteTextView? = null

    private var commandsBase: ArrayList<String> = ArrayList()
    private var commands: ArrayList<String> = ArrayList()
    private var adapter: ArrayAdapter<String>? = null

    private var loggerReporters:LoggerReporters? = null
    private var pythonReporter:LoggerReporter? = null
    private var activityReporter:LoggerReporter? = null
    private var requestReporter:LoggerReporter?= null
    private var responseReporter:LoggerReporter? = null

    private var heavenClient : Client? = null

    override fun onCreate(savedInstanceState:Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setTitle(R.string.wait_heaven_address)
    }

    override fun onResume() {
        super.onResume()
        showDebug()
    }

    override fun onPause() {
        super.onPause()
        hideDebug()
    }

    override fun onDestroy() {
        super.onDestroy()
        hideDebug()
    }

    override fun onCreateOptionsMenu(menu: Menu):Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.activity_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem):Boolean {
        when (item.itemId) {
            R.id.start_service -> startHeaven()

            R.id.stop_service -> stopHeaven()

            R.id.send_file -> getFilePath()
        }

        return super.onOptionsItemSelected(item)
    }

    private fun startHeaven() {
        Log.d(TAG, "Starting Heaven")
        if (ContextCompat.checkSelfPermission(applicationContext, WRITE_EXTERNAL_STORAGE)
            == PERMISSION_DENIED)
        {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(WRITE_EXTERNAL_STORAGE),
                REQUEST_START_HEAVEN)
            return
        }

        // TODO Check if this remnant client do not generate issues
        if (heavenClient == null) {
            heavenClient = Client(applicationContext, 9111)
        }
        heavenClient!!.setHeavenReplies(heavenReplies)
        commandsBase = ArrayList(HeavenCommands.commands.toList())
        updateCommands(ArrayList())
    }

    private fun stopHeaven() {
        Client.stopHeaven()
        heavenClient?.stopClient()
        heavenClient = null
    }



    private fun getFilePath() {
        // ACTION_OPEN_DOCUMENT is the intent to choose a file via the system's file
        // browser.
        val intent = Intent(Intent.ACTION_OPEN_DOCUMENT)

        // Filter to only show results that can be "opened", such as a
        // file (as opposed to a list of contacts or timezones)
        intent.addCategory(Intent.CATEGORY_OPENABLE)

        // Filter to show only images, using the image MIME data type.
        // If one wanted to search for ogg vorbis files, the type would be "audio/ogg".
        // To search for all documents available via installed storage providers,
        // it would be "*/*".
        intent.type = "*/*"

        startActivityForResult(intent, READ_REQUEST_CODE)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (requestCode == REQUEST_START_HEAVEN) {
            for (i in permissions.indices) {
                if (grantResults[i] == PERMISSION_DENIED) {
                    return
                }
                startHeaven()
            }
        }
    }

    override fun onActivityResult(requestCode:Int, resultCode:Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == READ_REQUEST_CODE && resultCode == Activity.RESULT_OK)
        {
            if (data == null) return

            val uri = data.data
            if (uri == null || uri.path == null) return

            val path = UriToPath.getPathFromUri(this, uri) ?: return

            val file = File(path)
            if (!file.exists()) return

            val clipboardManager = getSystemService(CLIPBOARD_SERVICE) as ClipboardManager

            val clipData = ClipData.newPlainText("filepath", file.absolutePath)
            clipboardManager.setPrimaryClip(clipData)
            runOnUiThread {
                Toast.makeText(
                    applicationContext,
                    file.absolutePath,
                    Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun postRequest(input:String) {
        Log.d(TAG, input)
        val command = input.split(" ".toRegex(), 2).toTypedArray()[0]
        val params:Array<String>
        when (command) {
            HeavenCommands.GET_HEAVEN_ADDRESS -> heavenClient!!.heavenAddress

            HeavenCommands.GET_PEERS -> heavenClient!!.peers

            HeavenCommands.GET_PEERS_UPDATES -> heavenClient!!.peersUpdate

            HeavenCommands.STOP_PEERS_UPDATE -> {
                params = input.split(" ".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                if (params.size == 2)
                    heavenClient!!.stopPeersUpdate(params[1])
                else
                    Log.d(TAG, HeavenCommands.STOP_PEERS_UPDATE + " <subscription_id>")
            }

            HeavenCommands.GET_NEIGHBORS -> heavenClient!!.neighbors

            HeavenCommands.GET_LSP -> {
                heavenClient!!.lsp
                heavenClient!!.routingTable
            }

            HeavenCommands.GET_ROUTING_TABLE -> heavenClient!!.routingTable

            HeavenCommands.GET_ROUTING_TREES -> heavenClient!!.routingTrees

            HeavenCommands.GET_FULL_NETWORK -> heavenClient!!.fullNetwork

            HeavenCommands.BLOCK_NEIGHBOR -> {
                params = input.split(" ".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                if (params.size == 2)
                    heavenClient!!.blockNeighbor(params[1])
                else
                    Log.d(TAG, HeavenCommands.BLOCK_NEIGHBOR + " <Peer Address>")
            }

            HeavenCommands.UNBLOCK_NEIGHBOR -> {
                params = input.split(" ".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                if (params.size == 2)
                    heavenClient!!.unblockNeighbor(params[1])
                else
                    Log.d(TAG, HeavenCommands.UNBLOCK_NEIGHBOR + " <Peer Address>")
            }

            HeavenCommands.GET_MY_IP_FOR_PEER -> {
                params = input.split(" ".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                if (params.size == 2)
                    heavenClient!!.getMyIpForPeer(params[1])
                else
                    Log.d(TAG, HeavenCommands.GET_MY_IP_FOR_PEER + " <Peer Address>")
            }

            HeavenCommands.GET_PEER_IP -> {
                params = input.split(" ".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                if (params.size == 2)
                    heavenClient!!.getPeerIp(params[1])
                else
                    Log.d(TAG, HeavenCommands.GET_PEER_IP + " <Peer Address>")
            }


            HeavenCommands.CREATE_TCP_SERVER -> {
                params = input.split(" ".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                try
                {
                    if (params.size == 2)
                        heavenClient!!.createTcpServer(Integer.parseInt(params[1]))
                    else
                        Log.d(TAG, "create_tcp_server <port>")
                }
                catch (e:NumberFormatException) {
                    e.printStackTrace()
                }

            }

            HeavenCommands.STOP_TCP_SERVER -> {
                params = input.split(" ".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                try
                {
                    if (params.size == 2)
                        heavenClient!!.stopTcpServer(Integer.parseInt(params[1]))
                    else
                        Log.d(TAG, "stop_tcp_server <port>")
                }
                catch (e:NumberFormatException) {
                    e.printStackTrace()
                }

            }

            HeavenCommands.GET_TCP_SERVERS -> heavenClient!!.tcpServers

            HeavenCommands.CREATE_TCP_SERVER_SOCKET -> {
                params = input.split(" ".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                try
                {
                    if (params.size == 2)
                        heavenClient!!.createTcpServerSocket(Integer.parseInt(params[1]))
                    else
                        Log.d(TAG, "create_tcp_server_socket <port>")
                }
                catch (e:NumberFormatException) {
                    e.printStackTrace()
                }

            }

            HeavenCommands.STOP_TCP_SERVER_SOCKET -> {
                params = input.split(" ".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                try
                {
                    if (params.size == 2)
                        heavenClient!!.stopTcpServerSocket(Integer.parseInt(params[1]))
                    else
                        Log.d(TAG, "stop_tcp_server_socket <port>")
                }
                catch (e:NumberFormatException) {
                    e.printStackTrace()
                }

            }

            HeavenCommands.GET_TCP_SERVERS_SOCKET -> heavenClient!!.tcpServersSocket

            HeavenCommands.CREATE_TCP_CLIENT -> {
                params = input.split(" ".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                try
                {
                    if (params.size == 4)
                    {
                        heavenClient!!.createTcpClient(
                            params[1],
                            Integer.parseInt(params[2]),
                            Integer.parseInt(params[3])
                        )
                    }
                    else
                        Log.d(TAG, "create_tcp_client <address> <port> <routing_method>")
                }
                catch (e:NumberFormatException) {
                    e.printStackTrace()
                }

            }

            HeavenCommands.SEND_TCP -> {
                params = input.split(" ".toRegex(), 3).toTypedArray()
                if (params.size == 3)
                {
                    heavenClient!!.sendTcp(params[1], params[2])
                }
                else
                {
                    Log.d(TAG, "send_tcp <session_id> <message>")
                }
            }

            HeavenCommands.SEND_TCP_FILE -> {
                params = input.split(" ".toRegex(), 4).toTypedArray()
                try
                {
                    if (params.size == 4)
                    {
                        heavenClient!!.sendTcpFile(
                            params[1],
                            Integer.parseInt(params[2]) != 0,
                            params[3]
                        )
                    }
                    else
                        Log.d(TAG, "send_tcp_file <session_id> <delete_after_send> <filepath>")
                }
                catch (e:NumberFormatException) {
                    e.printStackTrace()
                }

            }

            HeavenCommands.STOP_TCP_CLIENT -> {
                params = input.split(" ".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                if (params.size == 2)
                {
                    heavenClient!!.stopTcpClient(params[1])
                }
                else
                    Log.d(TAG, "stop_tcp_client <session_id>")
            }

            HeavenCommands.GET_TCP_CLIENTS -> heavenClient!!.tcpClients

            HeavenCommands.SEND_TCP_SOCKET -> {
                params = input.split(" ".toRegex(), 4).toTypedArray()
                try
                {
                    if (params.size == 4)
                    {
                        heavenClient!!.sendTcpSocket(
                            params[1],
                            Integer.parseInt(params[2]),
                            params[3]
                        )
                    }
                    else
                        Log.d(TAG, "send_tcp_socket <server_address> <server_port> <message>")
                }
                catch (e:NumberFormatException) {
                    e.printStackTrace()
                }

            }

            HeavenCommands.SEND_TCP_FILE_SOCKET -> {
                params = input.split(" ".toRegex(), 5).toTypedArray()
                try
                {
                    if (params.size == 5)
                    {
                        heavenClient!!.sendTcpFileSocket(
                            params[1],
                            Integer.parseInt(params[2]),
                            Integer.parseInt(params[3]) != 0,
                            params[4]
                        )
                    }
                    else
                        Log.d(TAG, "send_tcp_socket <server_address> <server_port> <delete_after_send> <message>")
                }
                catch (e:NumberFormatException) {
                    e.printStackTrace()
                }

            }

            HeavenCommands.CREATE_UDP_SERVER -> {
                params = input.split(" ".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                try
                {
                    if (params.size == 2)
                    {
                        heavenClient!!.createUdpServer(Integer.parseInt(params[1]))
                    }
                    else
                        Log.d(TAG, "create_udp_server <port>")
                }
                catch (e:NumberFormatException) {
                    e.printStackTrace()
                }

            }

            HeavenCommands.STOP_UDP_SERVER -> {
                params = input.split(" ".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                try
                {
                    if (params.size == 2)
                    {
                        heavenClient!!.stopUdpServer(Integer.parseInt(params[1]))
                    }
                    else
                        Log.d(TAG, "stop_udp_server <port>")
                }
                catch (e:NumberFormatException) {
                    e.printStackTrace()
                }

            }

            HeavenCommands.GET_UDP_SERVERS -> heavenClient!!.udpServers

            HeavenCommands.CREATE_UDP_SERVER_SOCKET -> {
                params = input.split(" ".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                try
                {
                    if (params.size == 4)
                    {
                        heavenClient!!.createUdpServerSocket(
                            Integer.parseInt(params[1]),
                            Integer.parseInt(params[2]),
                            Integer.parseInt(params[3])
                        )
                    }
                    else
                        Log.d(TAG, "create_udp_server_socket <port> <rx_buffer_size> <rx_queue_len>")
                }
                catch (e:NumberFormatException) {
                    e.printStackTrace()
                }

            }

            HeavenCommands.STOP_UDP_SERVER_SOCKET -> {
                params = input.split(" ".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                try
                {
                    if (params.size == 2)
                    {
                        heavenClient!!.stopUdpServerSocket(Integer.parseInt(params[1]))
                    }
                    else
                        Log.d(TAG, "stop_udp_server_socket <port>")
                }
                catch (e:NumberFormatException) {
                    e.printStackTrace()
                }

            }

            HeavenCommands.GET_UDP_SERVERS_SOCKET -> heavenClient!!.udpServersSocket

            HeavenCommands.CREATE_UDP_CLIENT -> {
                params = input.split(" ".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                try
                {
                    if (params.size == 7)
                    {
                        heavenClient!!.createUdpClient(
                            params[1],
                            Integer.parseInt(params[2]),
                            Integer.parseInt(params[3]),
                            Integer.parseInt(params[4]),
                            Integer.parseInt(params[5]),
                            Integer.parseInt(params[6])
                        )
                    }
                    else
                        Log.d(TAG, "create_udp_client <server_address> <server_port> <mut> <max_hop> <packet_rate> <routing_method>")
                }
                catch (e:NumberFormatException) {
                    e.printStackTrace()
                }

            }

            HeavenCommands.SEND_UDP -> {
                params = input.split(" ".toRegex(), 3).toTypedArray()
                try
                {
                    if (params.size == 3)
                    {
                        heavenClient!!.sendUdp(
                            Integer.parseInt(params[1]),
                            params[2]
                        )
                    }
                    else
                        Log.d(TAG, "send_udp <source_port> <message>")
                }
                catch (e:NumberFormatException) {
                    e.printStackTrace()
                }

            }

            HeavenCommands.STOP_UDP_CLIENT -> {
                params = input.split(" ".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                try
                {
                    if (params.size == 2)
                    {
                        heavenClient!!.stopUdpClient(Integer.parseInt(params[1]))
                    }
                    else
                        Log.d(TAG, "stop_udp_client <source_port>")
                }
                catch (e:NumberFormatException) {
                    e.printStackTrace()
                }

            }

            HeavenCommands.GET_UDP_CLIENTS -> heavenClient!!.udpClients

            HeavenCommands.CREATE_UDP_CLIENT_SOCKET -> {
                params = input.split(" ".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                try
                {
                    if (params.size == 4)
                    {
                        heavenClient!!.createUdpClientSocket(
                            params[1],
                            Integer.parseInt(params[2]),
                            Integer.parseInt(params[3]).toLong()
                        )
                    }
                    else
                        Log.d(TAG, "create_udp_client_socket <server_address> <server_port> <tx_timeout>")
                }
                catch (e:NumberFormatException) {
                    e.printStackTrace()
                }

            }

            HeavenCommands.SEND_UDP_SOCKET -> {
                params = input.split(" ".toRegex(), 3).toTypedArray()
                try
                {
                    if (params.size == 3)
                    {
                        heavenClient!!.sendUdpSocket(
                            params[1],
                            params[2]
                        )
                    }
                    else
                        Log.d(TAG, "send_udp_socket <client_id> <message>")
                }
                catch (e:NumberFormatException) {
                    e.printStackTrace()
                }

            }

            HeavenCommands.STOP_UDP_CLIENT_SOCKET -> {
                params = input.split(" ".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                try
                {
                    if (params.size == 2)
                    {
                        heavenClient!!.stopUdpClientSocket(params[1])
                    }
                    else
                        Log.d(TAG, "stop_udp_client_socket <client_id>")
                }
                catch (e:NumberFormatException) {
                    e.printStackTrace()
                }

            }

            HeavenCommands.GET_UDP_CLIENTS_SOCKET -> heavenClient!!.udpClientsSocket

            HeavenCommands.STOP_HEAVEN -> stopHeaven()
        }
    }

    @SuppressLint("ClickableViewAccessibility", "RestrictedApi")
    internal fun showDebug() {
        sv = findViewById(R.id.s_log)
        lv = findViewById(R.id.l_log)
        fb = findViewById(R.id.f_log)

        pythonCommandsMulti = findViewById(R.id.python_commands_multi)
        pythonCommandsMulti!!.setTokenizer(SpaceTokenizer())

        pythonReporter = LoggerReporter("PL")
        pythonReporter!!.setContext(applicationContext)
        pythonReporter!!.setFilters(String.format("%s:V *:S", "Python"))
        pythonReporter!!.setUi(sv, lv)
        pythonReporter!!.setTextColor(R.color.python)

        activityReporter = LoggerReporter("AL")
        activityReporter!!.setContext(applicationContext)
        activityReporter!!.setFilters(String.format("%s:V *:S", TAG))
        activityReporter!!.setUi(sv, lv)
        activityReporter!!.setTextColor(R.color.heaven_service)


        requestReporter = LoggerReporter("RQL")
        requestReporter!!.setContext(applicationContext)
        requestReporter!!.setFilters(String.format("%s:V *:S", Client.TAG_HEAVEN_REQUEST))
        requestReporter!!.setUi(sv, lv)
        requestReporter!!.setTextColor(R.color.heaven_request)


        responseReporter = LoggerReporter("RSL")
        responseReporter!!.setContext(applicationContext)
        responseReporter!!.setFilters(String.format("%s:V *:S", Client.TAG_HEAVEN_RESPONSE))
        responseReporter!!.setUi(sv, lv)
        responseReporter!!.setTextColor(R.color.heaven_replies)

        loggerReporters = LoggerReporters()
        loggerReporters!!.addLogger(pythonReporter)
        loggerReporters!!.addLogger(activityReporter)
        loggerReporters!!.addLogger(requestReporter)
        loggerReporters!!.addLogger(responseReporter)

        loggerReporters!!.startLoggers()

        fb!!.setOnClickListener { v ->
            loggerReporters!!.startScrollLoggers()
            v.visibility = View.GONE
        }

        sv!!.setOnTouchListener { _, _ ->
            loggerReporters!!.stopScrollLoggers()
            fb!!.visibility = View.VISIBLE
            false
        }

        pythonCommandsMulti!!.setOnKeyListener(View.OnKeyListener { v, keyCode, event ->
            if ((event.action == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER)) {
                val command = pythonCommandsMulti!!.text.toString()
                if (command.isEmpty()) return@OnKeyListener false
                postRequest(command)
                pythonCommandsMulti!!.text.clear()
            }
            false
        })
    }

    private fun hideDebug() {
        loggerReporters!!.stopLoggers()
    }

    internal fun updateCommands(peers: ArrayList<String>) {
        commands.clear()
        commands.addAll(commandsBase)
        commands.addAll(peers)
        adapter = ArrayAdapter(this, android.R.layout.simple_dropdown_item_1line, commands)
        adapter!!.notifyDataSetChanged()
        pythonCommandsMulti!!.setAdapter(adapter)
    }

    private var heavenReplies:HeavenReplies = object:HeavenReplies() {
        override fun replyGetHeavenAddress(requestId:String, address:String, errorDescription:String) {
            Log.d(TAG, String.format(
                Locale.CANADA,
                "get_heaven_address\n" +
                        "request_id: %s\n" +
                        "address: %s\n" +
                        "error_description: %s",
                requestId, address, errorDescription
            ))
            title = address
        }

        override fun replyGetPeers(requestId:String, peers: ArrayList<String>, errorDescription:String) {
            Log.d(TAG, String.format(
                Locale.CANADA,
                ("get_peers\n" +
                        "request_id: %s\n" +
                        "peers: %s\n" +
                        "error_description: %s"),
                requestId, peers.toTypedArray().contentToString(), errorDescription
            ))

            updateCommands(peers)
        }

        override fun replyGetPeersUpdate(requestId:String, subscription:Boolean, subscriptionId:String, errorDescription:String) {
            Log.d(TAG, String.format(
                Locale.CANADA,
                ("get_peers_update\n" +
                        "request_id: %s\n" +
                        "subscription: %s\n" +
                        "subscription_id: %s\n" +
                        "error_description: %s"),
                requestId, subscription, subscriptionId, errorDescription
            ))
        }

        override fun replyStopPeersUpdate(requestId:String, unsubscription:Boolean, subscriptionId:String, errorDescription:String) {
            Log.d(TAG, String.format(
                Locale.CANADA,
                ("get_peers_update\n" +
                        "request_id: %s\n" +
                        "subscription: %s\n" +
                        "subscription_id: %s\n" +
                        "error_description: %s"),
                requestId, unsubscription, subscriptionId, errorDescription
            ))

        }

        override fun replyGetNeighbors(requestId:String, neighbors: ArrayList<String>, errorDescription:String) {
            Log.d(TAG, String.format(
                Locale.CANADA,
                ("get_neighbors\n" +
                        "request_id: %s\n" +
                        "neighbors: %s\n" +
                        "error_description: %s"),
                requestId, neighbors.toTypedArray().contentToString(), errorDescription
            ))
        }

        override fun replyGetLsp(requestId:String, lsp:String, errorDescription:String) {
            Log.d(TAG, String.format(
                Locale.CANADA,
                ("get_lsp\n" +
                        "request_id: %s\n" +
                        "lsp: %s\n" +
                        "error_description: %s"),
                requestId, lsp, errorDescription
            ))
        }

        override fun replyGetRoutingTable(requestId:String, routingTable:String, errorDescription:String) {
            Log.d(TAG, String.format(
                Locale.CANADA,
                ("get_routing_table\n" +
                        "request_id: %s\n" +
                        "routing_table: %s\n" +
                        "error_description: %s"),
                requestId, routingTable, errorDescription
            ))
        }

        override fun replyGetRoutingTrees(requestId:String, routingTrees:String, errorDescription:String) {
            Log.d(TAG, String.format(
                Locale.CANADA,
                ("get_routing_trees\n" +
                        "request_id: %s\n" +
                        "address: %s\n" +
                        "error_description: %s"),
                requestId, routingTrees, errorDescription
            ))
        }

        override fun replyGetFullNetwork(requestId:String, fullNetwork:String, errorDescription:String) {
            Log.d(TAG, String.format(
                Locale.CANADA,
                ("get_full_network\n" +
                        "request_id: %s\n" +
                        "address: %s\n" +
                        "error_description: %s"),
                requestId, fullNetwork, errorDescription
            ))
        }

        override fun replyBlockNeighbor(requestId:String, neighborAddress:String, blocked:Boolean, errorDescription:String) {
            Log.d(TAG, String.format(
                Locale.CANADA,
                ("get_full_network\n" +
                        "request_id: %s\n" +
                        "peer_address: %s\n" +
                        "blocked: %s\n" +
                        "error_description: %s"),
                requestId, neighborAddress, blocked, errorDescription
            ))
        }

        override fun replyUnblockNeighbor(requestId:String, neighborAddress:String, unblocked:Boolean, errorDescription:String) {
            Log.d(TAG, String.format(
                Locale.CANADA,
                ("get_full_network\n" +
                        "request_id: %s\n" +
                        "peer_address: %s\n" +
                        "unblocked: %s\n" +
                        "error_description: %s"),
                requestId, neighborAddress, unblocked, errorDescription
            ))
        }

        override fun replyGetPeerIp(requestId:String, peerAddress:String, peerIp:String, errorDescription:String) {
            Log.d(TAG, String.format(
                Locale.CANADA,
                ("get_full_network\n" +
                        "request_id: %s\n" +
                        "peer_address: %s\n" +
                        "peer_ip: %s\n" +
                        "error_description: %s"),
                requestId, peerAddress, peerIp, errorDescription
            ))
        }

        override fun replyGetMyIpForPeer(requestId:String, peerAddress:String, myIp:String, errorDescription:String) {
            Log.d(TAG, String.format(
                Locale.CANADA,
                ("get_full_network\n" +
                        "request_id: %s\n" +
                        "peer_address: %s\n" +
                        "my_ip: %s\n" +
                        "error_description: %s"),
                requestId, peerAddress, myIp, errorDescription
            ))
        }

        override fun replyCreateTcpServer(requestId:String, port:Int, created:Boolean, errorDescription:String) {
            Log.d(TAG, String.format(
                Locale.CANADA,
                ("create_tcp_server\n" +
                        "request_id: %s\n" +
                        "port: %d\n" +
                        "created: %s\n" +
                        "error_description: %s"),
                requestId, port, java.lang.Boolean.toString(created), errorDescription
            ))
        }

        override fun replyTcpServerReceivedMessage(requestId:String, port:Int, source:String, sessionId:String, message:String, messageLength:Int, elapsedTime:Long, bps:Int, errorDescription:String) {
            Log.d(TAG, String.format(
                Locale.CANADA,
                ("tcp_server_received_message\n" +
                        "request_id: %s\n" +
                        "port: %d\n" +
                        "source: %s\n" +
                        "session_id: %s\n" +
                        "message: %s\n" +
                        "message_length: %d\n" +
                        "elapsed_time: %d\n" +
                        "bps: %d\n" +
                        "error_description: %s"),
                requestId, port, source, sessionId, message, messageLength,
                elapsedTime, bps, errorDescription
            ))
        }

        override fun replyTcpServerReceivedFile(requestId:String, port:Int, source:String, sessionId:String, filePath:String, fileSize:Int, elapsedTime:Long, bps:Int, errorDescription:String) {
            Log.d(TAG, String.format(
                Locale.CANADA,
                ("tcp_server_received_file\n" +
                        "request_id: %s\n" +
                        "port: %d\n" +
                        "source: %s\n" +
                        "session_id: %s\n" +
                        "file_path: %s\n" +
                        "file_size: %d\n" +
                        "elapsed_time: %d\n" +
                        "bps: %d\n" +
                        "error_description: %s"),
                requestId, port, source, sessionId, filePath, fileSize,
                elapsedTime, bps, errorDescription
            ))
        }

        override fun replyStopTcpServer(requestId:String, port:Int, stopped:Boolean, errorDescription:String) {
            Log.d(TAG, String.format(
                Locale.CANADA,
                ("stop_tcp_server\n" +
                        "request_id: %s\n" +
                        "port: %d\n" +
                        "stopped: %s\n" +
                        "error_description: %s"),
                requestId, port, stopped, errorDescription
            ))
        }

        override fun replyGetTcpServers(requestId:String, servers: ArrayList<String>, errorDescription:String) {
            Log.d(TAG, String.format(
                Locale.CANADA,
                ("get_tcp_servers\n" +
                        "request_id: %s\n" +
                        "servers: %s\n" +
                        "error_description: %s"),
                requestId, servers.toTypedArray().contentToString(), errorDescription
            ))
        }

        override fun replyCreateTcpServerSocket(requestId:String, port:Int, created:Boolean, errorDescription:String) {
            Log.d(TAG, String.format(
                Locale.CANADA,
                ("create_tcp_server_socket\n" +
                        "request_id: %s\n" +
                        "port: %d\n" +
                        "created: %s\n" +
                        "error_description: %s"),
                requestId, port, java.lang.Boolean.toString(created), errorDescription
            ))
        }

        override fun replyTcpServerSocketReceivedMessage(requestId:String, port:Int, source:String, sessionId:String, message:String, messageLength:Int, elapsedTime:Long, bps:Int, errorDescription:String) {
            Log.d(TAG, String.format(
                Locale.CANADA,
                ("tcp_server_socket_received_message\n" +
                        "request_id: %s\n" +
                        "port: %d\n" +
                        "source: %s\n" +
                        "session_id: %s\n" +
                        "message: %s\n" +
                        "message_length: %d\n" +
                        "elapsed_time: %d\n" +
                        "bps: %d\n" +
                        "error_description: %s"),
                requestId, port, source, sessionId, message, messageLength,
                elapsedTime, bps, errorDescription
            ))
        }

        override fun replyTcpServerSocketReceivedFile(requestId:String, port:Int, source:String, sessionId:String, filePath:String, fileSize:Int, elapsedTime:Long, bps:Int, errorDescription:String) {
            Log.d(TAG, String.format(
                Locale.CANADA,
                ("tcp_server_socket_received_file\n" +
                        "request_id: %s\n" +
                        "port: %d\n" +
                        "source: %s\n" +
                        "session_id: %s\n" +
                        "file_path: %s\n" +
                        "file_size: %d\n" +
                        "elapsed_time: %d\n" +
                        "bps: %d\n" +
                        "error_description: %s"),
                requestId, port, source, sessionId, filePath, fileSize,
                elapsedTime, bps, errorDescription
            ))
        }

        override fun replyStopTcpServerSocket(requestId:String, port:Int, stopped:Boolean, errorDescription:String) {
            Log.d(TAG, String.format(
                Locale.CANADA,
                ("stop_tcp_server_socket\n" +
                        "request_id: %s\n" +
                        "port: %d\n" +
                        "stopped: %s\n" +
                        "error_description: %s"),
                requestId, port, stopped, errorDescription
            ))
        }

        override fun replyGetTcpServersSocket(requestId:String, servers: ArrayList<String>, errorDescription:String) {
            Log.d(TAG, String.format(
                Locale.CANADA,
                ("get_tcp_servers_socket\n" +
                        "request_id: %s\n" +
                        "servers: %s\n" +
                        "error_description: %s"),
                requestId, servers.toTypedArray().contentToString(), errorDescription
            ))
        }

        override fun replyCreateTcpClient(requestId:String, serverAddress:String, port:Int, sessionId:String, accepted:Boolean, errorDescription:String) {
            Log.d(TAG, String.format(
                Locale.CANADA,
                ("create_tcp_client\n" +
                        "request_id: %s\n" +
                        "server_address: %s\n" +
                        "port: %d\n" +
                        "session_id: %s\n" +
                        "accepted: %s\n" +
                        "error_description: %s"),
                requestId, serverAddress, port, sessionId, java.lang.Boolean.toString(accepted), errorDescription
            ))
        }

        override fun replyTcpSessionEstablished(requestId:String, serverAddress:String, port:Int, sessionId:String, created:Boolean, errorDescription:String) {
            Log.d(TAG, String.format(
                Locale.CANADA,
                ("tcp_session_established\n" +
                        "request_id: %s\n" +
                        "server_address: %s\n" +
                        "port: %d\n" +
                        "session_id: %s\n" +
                        "created: %s\n" +
                        "error_description: %s"),
                requestId, serverAddress, port, sessionId,
                java.lang.Boolean.toString(created), errorDescription
            ))
        }

        override fun replySendTcp(requestId:String, sessionId:String, messageId:String, accepted:Boolean, errorDescription:String) {
            Log.d(TAG, String.format(
                Locale.CANADA,
                ("send_tcp\n" +
                        "request_id: %s\n" +
                        "session_id: %s\n" +
                        "message_id: %s\n" +
                        "accepted: %s\n" +
                        "error_description: %s"),
                requestId, sessionId, messageId, java.lang.Boolean.toString(accepted), errorDescription
            )
            )
        }

        override fun replySendTcpFile(requestId:String, sessionId:String, messageId:String, accepted:Boolean, errorDescription:String) {
            Log.d(TAG, String.format(
                Locale.CANADA,
                ("send_tcp_file\n" +
                        "request_id: %s\n" +
                        "session_id: %s\n" +
                        "message_id: %s\n" +
                        "accepted: %s\n" +
                        "error_description: %s"),
                requestId, sessionId, messageId, java.lang.Boolean.toString(accepted), errorDescription
            )
            )
        }

        override fun replyTcpSessionAbort(requestId:String, serverAddress:String, serverPort:Int, sessionId:String, error:Int, errorDescription:String) {
            Log.d(TAG, String.format(
                Locale.CANADA,
                ("tcp_session_abort\n" +
                        "request_id: %s\n" +
                        "server_address: %s\n" +
                        "server_port: %d\n" +
                        "session_id: %s\n" +
                        "error: %d\n" +
                        "error_description: %s"),
                requestId, serverAddress, serverPort, sessionId, error, errorDescription
            )
            )
        }

        override fun replyTcpMessageSent(requestId:String, sessionId:String, messageId:String, sent:Boolean, fileSize:Int, elapsedTime:Long, bps:Int, errorDescription:String) {
            Log.d(TAG, String.format(
                Locale.CANADA,
                ("tcp_message_sent\n" +
                        "request_id: %s\n" +
                        "session_id: %s\n" +
                        "message_id: %s\n" +
                        "sent: %s\n" +
                        "file_size: %d\n" +
                        "elapsed_time: %d\n" +
                        "bps: %d\n" +
                        "error_description: %s"),
                requestId, sessionId, messageId, java.lang.Boolean.toString(sent), fileSize,
                elapsedTime, bps, errorDescription
            )
            )
        }

        override fun replyStopTcpClient(requestId:String, sessionId:String, stopped:Boolean, errorDescription:String) {
            Log.d(TAG, String.format(
                Locale.CANADA,
                ("stop_tcp_client\n" +
                        "request_id: %s\n" +
                        "session_id: %s\n" +
                        "stopped: %s\n" +
                        "error_description: %s"),
                requestId, sessionId, java.lang.Boolean.toString(stopped), errorDescription
            )
            )
        }

        override fun replyGetTcpClients(requestId:String, clients: ArrayList<String>, errorDescription:String) {
            Log.d(TAG, String.format(
                Locale.CANADA,
                ("get_tcp_clients\n" +
                        "request_id: %s\n" +
                        "clients: %s\n" +
                        "error_description: %s"),
                requestId, clients.toTypedArray().contentToString(), errorDescription
            )
            )
        }

        override fun replySendTcpSocket(requestId:String, sessionId:String, accepted:Boolean, errorDescription:String) {
            Log.d(TAG, String.format(
                Locale.CANADA,
                ("send_tcp_socket\n" +
                        "request_id: %s\n" +
                        "session_id: %s\n" +
                        "accepted: %s\n" +
                        "error_description: %s"),
                requestId, sessionId, java.lang.Boolean.toString(accepted), errorDescription
            )
            )
        }

        override fun replyTcpSocketMessageSent(requestId:String, sessionId:String, sent:Boolean, fileSize:Int, elapsedTime:Long, bps:Int, errorDescription:String) {
            Log.d(TAG, String.format(
                Locale.CANADA,
                ("tcp_socket_message_sent\n" +
                        "request_id: %s\n" +
                        "session_id: %s\n" +
                        "sent: %s\n" +
                        "file_size: %d\n" +
                        "elapsed_time: %d\n" +
                        "bps: %d\n" +
                        "error_description: %s"),
                requestId, sessionId, java.lang.Boolean.toString(sent), fileSize,
                elapsedTime, bps, errorDescription
            )
            )
        }

        override fun replyTcpSocketError(requestId:String, serverAddress:String, serverPort:Int, sessionId:String, error:Int, errorDescription:String) {
            Log.d(TAG, String.format(
                Locale.CANADA,
                ("tcp_socket_error\n" +
                        "request_id: %s\n" +
                        "server_address: %s\n" +
                        "server_port: %d\n" +
                        "session_id: %s\n" +
                        "error: %d\n" +
                        "error_description: %s"),
                requestId, serverAddress, serverPort, sessionId, error, errorDescription
            )
            )
        }

        override fun replyCreateUdpServer(requestId:String, port:Int, created:Boolean, errorDescription:String) {
            Log.d(TAG, String.format(
                Locale.CANADA,
                ("create_udp_server\n" +
                        "request_id: %s\n" +
                        "port: %d\n" +
                        "created: %s\n" +
                        "error_description: %s"),
                requestId, port, java.lang.Boolean.toString(created), errorDescription
            ))
        }

        override fun replyUdpServerReceivedMessage(requestId:String, port:Int, source:String, sourcePort:Int, message:String, errorDescription:String) {
            Log.d(TAG, String.format(
                Locale.CANADA,
                ("udp_server_received_message\n" +
                        "request_id: %s\n" +
                        "port: %d\n" +
                        "source: %s\n" +
                        "source_port: %s\n" +
                        "message: %s\n" +
                        "error_description: %s"),
                requestId, port, source, sourcePort, message, errorDescription
            ))
        }

        override fun replyStopUdpServer(requestId:String, port:Int, stopped:Boolean, errorDescription:String) {
            Log.d(TAG, String.format(
                Locale.CANADA,
                ("stop_udp_server\n" +
                        "request_id: %s\n" +
                        "port: %d\n" +
                        "stopped: %s\n" +
                        "error_description: %s"),
                requestId, port, stopped, errorDescription
            ))
        }

        override fun replyGetUdpServers(requestId:String, servers: ArrayList<String>, errorDescription:String) {
            Log.d(TAG, String.format(
                Locale.CANADA,
                ("get_udp_servers\n" +
                        "request_id: %s\n" +
                        "servers: %s\n" +
                        "error_description: %s"),
                requestId, servers.toTypedArray().contentToString(), errorDescription
            ))
        }

        override fun replyCreateUdpServerSocket(requestId:String, port:Int, created:Boolean, errorDescription:String) {
            Log.d(TAG, String.format(
                Locale.CANADA,
                ("create_udp_server_socket\n" +
                        "request_id: %s\n" +
                        "port: %d\n" +
                        "created: %s\n" +
                        "error_description: %s"),
                requestId, port, java.lang.Boolean.toString(created), errorDescription
            ))
        }

        override fun replyUdpServerSocketReceivedMessage(requestId:String, port:Int, source:String, sourcePort:Int, message:String, errorDescription:String) {
            Log.d(TAG, String.format(
                Locale.CANADA,
                ("udp_server_socket_received_message\n" +
                        "request_id: %s\n" +
                        "port: %d\n" +
                        "source: %s\n" +
                        "source_port: %s\n" +
                        "message: %s\n" +
                        "error_description: %s"),
                requestId, port, source, sourcePort, message, errorDescription
            ))
        }

        override fun replyStopUdpServerSocket(requestId:String, port:Int, stopped:Boolean, errorDescription:String) {
            Log.d(TAG, String.format(
                Locale.CANADA,
                ("stop_udp_server_socket\n" +
                        "request_id: %s\n" +
                        "port: %d\n" +
                        "stopped: %s\n" +
                        "error_description: %s"),
                requestId, port, stopped, errorDescription
            ))
        }

        override fun replyGetUdpServersSocket(requestId:String, servers: ArrayList<String>, errorDescription:String) {
            Log.d(TAG, String.format(
                Locale.CANADA,
                ("get_udp_servers_socket\n" +
                        "request_id: %s\n" +
                        "servers: %s\n" +
                        "error_description: %s"),
                requestId, servers.toTypedArray().contentToString(), errorDescription
            ))
        }

        override fun replyCreateUdpClient(requestId:String, serverAddress:String, serverPort:Int, sourcePort:String, created:Boolean, errorDescription:String) {
            Log.d(TAG, String.format(
                Locale.CANADA,
                ("create_udp_client\n" +
                        "request_id: %s\n" +
                        "server_address: %s\n" +
                        "server_port: %d\n" +
                        "source_port: %s\n" +
                        "created: %s\n" +
                        "error_description: %s"),
                requestId, serverAddress, serverPort, sourcePort, java.lang.Boolean.toString(created), errorDescription
            ))
        }

        override fun replySendUdp(requestId:String, sourcePort:String, sent:Boolean, errorDescription:String) {
            Log.d(TAG, String.format(
                Locale.CANADA,
                ("send_udp\n" +
                        "request_id: %s\n" +
                        "source_port: %s\n" +
                        "sent: %s\n" +
                        "error_description: %s"),
                requestId, sourcePort, java.lang.Boolean.toString(sent), errorDescription
            )
            )
        }

        override fun replyStopUdpClient(requestId:String, sourcePort:String, stopped:Boolean, errorDescription:String) {
            Log.d(TAG, String.format(
                Locale.CANADA,
                ("stop_udp_client\n" +
                        "request_id: %s\n" +
                        "source_port: %s\n" +
                        "stopped: %s\n" +
                        "error_description: %s"),
                requestId, sourcePort, java.lang.Boolean.toString(stopped), errorDescription
            )
            )
        }

        override fun replyGetUdpClients(requestId:String, clients: ArrayList<String>, errorDescription:String) {
            Log.d(TAG, String.format(
                Locale.CANADA,
                ("get_udp_clients\n" +
                        "request_id: %s\n" +
                        "clients: %s\n" +
                        "error_description: %s"),
                requestId, clients.toTypedArray().contentToString(), errorDescription
            )
            )
        }

        override fun replyCreateUdpClientSocket(requestId:String, serverAddress:String, serverPort:Int, sourcePort:String, created:Boolean, errorDescription:String) {
            Log.d(TAG, String.format(
                Locale.CANADA,
                ("create_udp_client\n" +
                        "request_id: %s\n" +
                        "server_address: %s\n" +
                        "server_port: %d\n" +
                        "source_port: %s\n" +
                        "created: %s\n" +
                        "error_description: %s"),
                requestId, serverAddress, serverPort, sourcePort, java.lang.Boolean.toString(created), errorDescription
            ))
        }

        override fun replySendUdpSocket(requestId:String, sourcePort:String, sent:Boolean, errorDescription:String) {
            Log.d(TAG, String.format(
                Locale.CANADA,
                ("send_udp_socket\n" +
                        "request_id: %s\n" +
                        "source_port: %s\n" +
                        "sent: %s\n" +
                        "error_description: %s"),
                requestId, sourcePort, java.lang.Boolean.toString(sent), errorDescription
            )
            )
        }

        override fun replyStopUdpClientSocket(requestId:String, sourcePort:String, stopped:Boolean, errorDescription:String) {
            Log.d(TAG, String.format(
                Locale.CANADA,
                ("stop_udp_client_socket\n" +
                        "request_id: %s\n" +
                        "source_port: %s\n" +
                        "stopped: %s\n" +
                        "error_description: %s"),
                requestId, sourcePort, java.lang.Boolean.toString(stopped), errorDescription
            )
            )
        }

        override fun replyGetUdpClientsSocket(requestId:String, clients: ArrayList<String>, errorDescription:String) {
            Log.d(TAG, String.format(
                Locale.CANADA,
                ("get_udp_clients_socket\n" +
                        "request_id: %s\n" +
                        "clients: %s\n" +
                        "error_description: %s"),
                requestId, clients.toTypedArray().contentToString(), errorDescription
            )
            )
        }
    }

    companion object {
        private const val TAG = "MA"
        private const val READ_REQUEST_CODE: Int = 42
        private const val REQUEST_START_HEAVEN = 43
    }
}
