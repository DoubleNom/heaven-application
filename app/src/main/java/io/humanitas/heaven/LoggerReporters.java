package io.humanitas.heaven;

import java.util.ArrayList;

public class LoggerReporters {
    private final ArrayList<LoggerReporter> loggers;

    LoggerReporters() {
        loggers = new ArrayList<>();
    }

    void addLogger(LoggerReporter logger) {
        loggers.add(logger);
    }

    void startLoggers() {
        for(LoggerReporter logger: loggers) {
            logger.startLogging();
        }
    }

    void stopLoggers() {
        for(LoggerReporter logger: loggers) {
            logger.cancel();
        }
    }

    void startScrollLoggers() {
        for(LoggerReporter logger: loggers) {
            logger.setUserScrolled(false);
        }
    }

    void stopScrollLoggers() {
        for(LoggerReporter logger: loggers) {
            logger.setUserScrolled(true);
        }
    }




}
