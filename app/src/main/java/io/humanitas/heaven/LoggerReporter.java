package io.humanitas.heaven;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

class LoggerReporter extends Thread {

    private boolean doRun;
    private Context context;
    private final Handler mainHandler;
    private LinearLayout layout;
    private ScrollView scrollView;
    private int textColor = Color.BLACK;
    private boolean userScrolled = false;
    private String filters = "";

    private static final int ALREADY_RUNNING = 1;
    private static final int CONTEXT_MISSING = 2;
    private static final int LAYOUT_MISSING = 3;

    LoggerReporter(String name) {
        super(name);
        mainHandler = new Handler(Looper.getMainLooper());
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public void setTextColor(int color) {
        textColor = ContextCompat.getColor(context, color);
    }

    public void setUi(ScrollView scrollView, LinearLayout layout) {
        this.scrollView = scrollView;
        this.layout = layout;
    }

    public void setFilters(String filters) {
        this.filters = filters;
    }

    void setUserScrolled(boolean state) {
        userScrolled = state;
        if(!userScrolled) {
            mainHandler.post(new Runnable() {
                @Override
                public void run() {
                    scrollView.fullScroll(View.FOCUS_DOWN);
                }
            });
        }
    }

    @SuppressWarnings("UnusedReturnValue")
    int startLogging() {
        if (this.isAlive()) return ALREADY_RUNNING;
        if (context == null) return CONTEXT_MISSING;
        if (layout == null || scrollView == null) return LAYOUT_MISSING;

        this.start();
        return 0;
    }

    void cancel() {
        doRun = false;
    }

    private void updateLogger(String text) {
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
        );
        params.setMargins(8, 8, 8, 8);
        final TextView textView = new TextView(context);
        textView.setLayoutParams(params);
        textView.setTextColor(textColor);
        textView.setText(text);
        textView.setTypeface(Typeface.MONOSPACE);
        mainHandler.post(new Runnable() {
            @Override
            public void run() {
                layout.addView(textView);
                mainHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if(!userScrolled)
                            scrollView.fullScroll(View.FOCUS_DOWN);
                    }
                }, 100);
            }
        });
    }

    @Override
    public void run() {
        super.run();
        doRun = true;

        try {
            Runtime.getRuntime().exec("logcat -c");
            String lCommand = String.format("logcat -b main -v raw%s", filters == null ? "" : " " + filters);
            Process process = Runtime.getRuntime().exec(lCommand);
            final BufferedReader bufferedReader = new BufferedReader(
                    new InputStreamReader(process.getInputStream()));
            String line;

            while (doRun) {
                line = bufferedReader.readLine();
                if (line == null) {
                    Thread.sleep(100);
                    continue;
                }
                updateLogger(line);
            }
        } catch(IOException | InterruptedException e){
            e.printStackTrace();
        }
    }
}
